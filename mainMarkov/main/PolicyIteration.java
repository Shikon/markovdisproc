package main;

import java.util.Random;

/**
 * This is the template of a class that should run policy iteration on a given
 * MDP to compute the optimal policy which is returned in the public
 * <tt>policy</tt> field. You need to fill in the constructor. You may wish to
 * add other fields with other useful information that you want this class to
 * return (for instance, number of iterations before convergence). You also may
 * add other constructors or methods, provided that these are in addition to the
 * one given below (which is the one that will be automatically tested). In
 * particular, your code must work properly when run with the <tt>main</tt>
 * provided in <tt>RunCatMouse.java</tt>.
 */
public class PolicyIteration {

	/** the computed optimal policy for the given MDP **/
	public int policy[];

	private Mdp mdp;
	private double discount;
	private int bestMove;
	private double utility[];
	


	/**
	 * The constructor for this class. Computes the optimal policy for the given
	 * <tt>mdp</tt> with given <tt>discount</tt> factor, and stores the answer
	 * in <tt>policy</tt>.
	 */
	public PolicyIteration(Mdp mdp, double discount) {

		this.mdp = mdp;
		this.discount = discount;
		policy = new int[mdp.numStates];
		computeIteration();

	}

	private void computeIteration() {

		this.utility = new double[mdp.numStates];

		boolean noChange = false;
		double currUtility = 0;
		double bestUtility = 0;

		// Initally setting random policy
		setInitalRandomPolicy();

	/*	for(int p:policy){
			System.out.println(p);
		}*/
		do{

			noChange = false;
			
			PolicyEvaluation pe = new PolicyEvaluation(mdp,discount,policy);
			double tempUtility[] = pe.utility;
			
			for( int i = 0 ; i < mdp.numStates ; i++){
				utility[i] = tempUtility[i] ;
			}
		/*	
			for(double u:utility){
				System.out.println(u);
			}
*/
			for (int i = 0; i < mdp.numStates; i++) {

				currUtility = expectedUtility(i);
				bestUtility = expectedBestUtility(i);
				
				if(bestUtility> currUtility){
					policy[i] = getBestMove();
					noChange = true;
				}
			}
			
		

		} while(noChange);

	}



	private double expectedBestUtility(int state) {
		

		double bestUtility = -100000000; // strong very large negative number;
		double tempUtility;

		for (int i = 0; i < mdp.numActions; i++) { // for each action
		
			tempUtility = 0;
			
			for (int j = 0; j < mdp.nextState[state][i].length; j++) {

				// get the next state location for utility
				int nextState = mdp.nextState[state][i][j];

				double nextStateProb = mdp.transProb[state][i][j];
				double nextStateUtility = this.utility[nextState];
				tempUtility = tempUtility + (nextStateProb * nextStateUtility);
			}

			if (tempUtility > bestUtility) {
				bestUtility = tempUtility;
				setBestMove(i);
			}

		}

		return bestUtility;
	}

	private double expectedUtility(int state) {
		

		double totalUtility = 0;

		for (int i = 0; i < mdp.nextState[state][this.policy[state]].length; i++) {

			// get the next state location for utility
			int nextState = mdp.nextState[state][this.policy[state]][i];

			double nextStateProb = mdp.transProb[state][this.policy[state]][i];
			double nextStateUtility = this.utility[nextState];
			totalUtility = totalUtility + (nextStateProb * nextStateUtility);

		}

		return totalUtility;
	
	}

	
	public int getBestMove() {
		return bestMove;
	}

	public void setBestMove(int bestMove) {
		this.bestMove = bestMove;
	}
	private void setInitalRandomPolicy() {

		for (int i = 0; i < mdp.numStates; i++) {
			policy[i] = randInt(0, mdp.numActions);
		}
	}

	public int randInt(int min, int max) {
		Random rand = new Random();
		// add 1 to make it inclusive
		int randomNum = rand.nextInt(max - min) + min;
		return randomNum;
	}

}
