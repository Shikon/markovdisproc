package main;

/**
 * This is the template of a class that should run value iteration on a given
 * MDP to compute the optimal policy which is returned in the public
 * <tt>policy</tt> field. The computed optimal utility is also returned in the
 * public <tt>utility</tt> field. You need to fill in the constructor. You may
 * wish to add other fields with other useful information that you want this
 * class to return (for instance, number of iterations before convergence). You
 * also may add other constructors or methods, provided that these are in
 * addition to the one given below (which is the one that will be automatically
 * tested). In particular, your code must work properly when run with the
 * <tt>main</tt> provided in <tt>RunCatMouse.java</tt>.
 */
public class ValueIteration {

	/** the computed optimal policy for the given MDP **/
	public int policy[];

	/** the computed optimal utility for the given MDP **/
	public double utility[];

	private Mdp mdp;
	private double discount;

	/**
	 * The constructor for this class. Computes the optimal policy for the given
	 * <tt>mdp</tt> with given <tt>discount</tt> factor, and stores the answer
	 * in <tt>policy</tt>. Also stores the optimal utility in <tt>utility</tt>.
	 */
	public ValueIteration(Mdp mdp, double discount) {

		this.mdp = mdp;
		this.discount = discount;
		ComputeIteration();

	}

	private void ComputeIteration() {

		this.policy = new int[mdp.numStates];
		this.utility = new double[mdp.numStates];
		double newUtility[] = new double[mdp.numStates];
	
		this.utility = initializeUtility(utility);
		newUtility = initializeUtility(newUtility);

		double expected = 0;
		double compareUtility = 0;
		double delta = 0;	// Storing the biggest utility
		
		double error = (getThreshold() * (1 - discount)) / (double)(discount);
		
		do {

			delta = 0;
			updateNewutility(newUtility);

			for (int i = 0; i < mdp.numStates; i++) {

				expected = expectedUtility(i);
				newUtility[i] = reward(i) + (discount * expected);
				compareUtility = Math.abs(this.utility[i] - newUtility[i]);

				if (compareUtility > delta) {
					delta = compareUtility;
				}
			}

		} while (error < delta);
	}

	private double[] initializeUtility(double[] newUtility) {

		for (int i = 0; i < newUtility.length; i++) {
			newUtility[i] = 0;
		}
		return newUtility;

	}

	private void updateNewutility(double newUtility[]) {

		for (int i = 0; i < mdp.numStates; i++) {
			this.utility[i] = newUtility[i];
		}
	}

	private double reward(int state) {
		return mdp.reward[state];
	}

	private double getThreshold() {
		double threshold = Math.pow(10, -8);
		return threshold;
	}

	private double expectedUtility(int state) {

		int currentState = state;
		int bestPath = -1; // storing the best path;
		double bestUtility = -100000000; // strong best utility, very large negative number to make 
										 //sure first utility is very small than best utility
		double tempUtility = 0;

		for (int i = 0; i < mdp.numActions; i++) {

			for (int j = 0; j < mdp.nextState[currentState][i].length; j++) {

				// get the next state location for utility
				int nextState = mdp.nextState[currentState][i][j];

				double nextStateProb = mdp.transProb[currentState][i][j];
				double nextStateUtility = this.utility[nextState];
				tempUtility = tempUtility + (nextStateProb * nextStateUtility);

			}

			if (tempUtility > bestUtility) {
				bestUtility = tempUtility;
				bestPath = i;
			}

		}

		setNewPolicy(state, bestPath);

		return bestUtility;

	}

	private void setNewPolicy(int state, int bestPath) {
		this.policy[state] = bestPath;

	}

}
