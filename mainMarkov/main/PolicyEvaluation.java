package main;

/**
 * This is the template of a class that evaluates a given policy, i.e., computes
 * the utility of each state when actions are chosen according to it. The
 * utility is returned in the public <tt>utility</tt> field. You need to fill in
 * the constructor. You may wish to add other fields with other useful
 * information that you want this class to return (for instance, number of
 * iterations before convergence). You also may add other constructors or
 * methods, provided that these are in addition to the one given below (which is
 * the one that will be automatically tested). In particular, your code must
 * work properly when run with the <tt>main</tt> provided in
 * <tt>RunCatMouse.java</tt>.
 */
public class PolicyEvaluation {

	/** the computed utility of each state under the given policy */
	public double utility[];

	private Mdp mdp;
	private double discount;
	private int pi[];

	/**
	 * The constructor for this class. Computes the utility of policy
	 * <tt>pi</tt> for the given <tt>mdp</tt> with given <tt>discount</tt>
	 * factor, and stores the answer in <tt>utility</tt>.
	 */
	public PolicyEvaluation(Mdp mdp, double discount, int pi[]) {

		this.mdp = mdp;
		this.discount = discount;
		this.pi = pi;
		computeEvaluation();

	}

	private void computeEvaluation() {

		this.utility = new double[mdp.numStates];
		double currentUtility[] = new double[mdp.numStates];

		// this.utility = initializeUtility(this.utility);
		currentUtility = initializeUtility(currentUtility);

		double expected = 0;
		double compareUtility = 0;
		double delta = 0; // Storing the biggest utility

		double error = (getThreshold() * (1 - discount)) / (double) (discount);
		// System.out.println(error);

		do {
			delta = 0;

			currentUtility = updateCurrentutility(currentUtility);

			for (int i = 0; i < mdp.numStates; i++) {

				expected = expectedUtility(i);

				this.utility[i] = reward(i) + (discount * expected);

				// .out.print(this.utility[i]);

				compareUtility = Math.abs(this.utility[i] - currentUtility[i]);
				// System.out.print("\t"+currentUtility[i]);
				// System.out.println("\t"+compareUtility);
				if (compareUtility > delta) {
					delta = compareUtility;
					// System.out.println(delta);
				}
			}

			// System.out.println(delta);
		} while (error < delta);

	}

	private double expectedUtility(int state) {

		double totalUtility = 0;

		for (int i = 0; i < mdp.nextState[state][this.pi[state]].length; i++) {

			// get the next state location for utility
			int nextState = mdp.nextState[state][this.pi[state]][i];

			double nextStateProb = mdp.transProb[state][this.pi[state]][i];
			double nextStateUtility = this.utility[nextState];
			totalUtility += (nextStateProb * nextStateUtility);

		}

		return totalUtility;

	}

	private double[] updateCurrentutility(double currentUtility[]) {

		for (int i = 0; i < mdp.numStates; i++) {
			currentUtility[i] = this.utility[i];
		}
		return currentUtility;
	}

	private double[] initializeUtility(double[] utility) {

		for (int i = 0; i < utility.length; i++) {
			utility[i] = 0;
		}
		return utility;

	}

	private double reward(int state) {
		return mdp.reward[state];
	}

	private double getThreshold() {
		double threshold = Math.pow(10, -8);
		return threshold;
	}

}
